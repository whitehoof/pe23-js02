console.groupCollapsed("ТЕОРІЯ (click here to expand)");

// ТЕОРЕТИЧНІ ПИТАННЯ:



console.group("\nТЕОРІЯ — 1. Які існують типи даних у Javascript?\n\n");

console.log("ECMA та DAN.IT на сьогодні пропонують наступні типи даних в JS:");
let assignedButUnsetVariable;
console.log("1.", typeof assignedButUnsetVariable);
// console.log("1. " + typeof unassignedVariable); // ще один варіант отримати undefined
console.log("2.", null); // не знаю неявних способів отримати null
console.log("3.", typeof true);
console.log("4.", typeof "1");
console.log("5.", typeof Symbol()); // Практичне застосування типу symbol я не зрозумів.
console.log("6.", typeof 1);
console.log("7.", typeof 1n);
console.log("8.", typeof {});

console.groupEnd();



console.group("\nТЕОРІЯ — 2. У чому різниця між == і ===?\n\n");

console.log("Нестроге порівняння зводить порівнювані значення до одного типу: код 1 == \"1\" видасть", 1 == "1");
console.log("Строге порівняння не звонить порівнювані значення до одного типу: код 1 === \"1\" видасть",1 === "1"); // false

console.groupEnd();



console.group("\nТЕОРІЯ — 3. Що таке оператор?\n\n");

console.log("Це послідовність з одного чи більше символів, що виконує певну операцію над одним чи більше операндами і повертає результат. ПРИКЛАДИ:");
console.log("2 + 3\n", 2+3, "\nТут 2 та 3 — операнди, плюс — оператор, що виконує певну дію з двома операндами та видає результат цієї дії (математичне складання).");
console.log("2 && 0 && false\n", 2 && 0 && false, "\nТут двійка, нуль на false — операнди, а подвійні амперсанди — оператори, що виконують певну дію з парами операндів навколо себе та видають результати цих дій (логічне порівняння \"AND\").");
console.log("+\"25\"\n", +"25", "\nТут плюс перед строкою — оператор, шо виконує дію над строкою, перед якою він стоїть, а саме неявно перетворює string на number (по фіолетовому кольору в консолі бачимо, що перетворення вдалося).");
console.log("== та === теж є операторами (порівняльними), як і символи >, <, >=, <=, !=, !== та подібні.");

console.groupEnd();
console.groupEnd();




/*
ПРАКТИКА — СПРОБА 2:


Оскільки в завданні вказано: 
>	"Після введення даних...
>	Якщо користувач не ввів ім'я, 
>	_АБО_ 
>	при введенні віку вказав не число - 
>	запитати ім'я та вік наново",
тоді виходить, що спочатку треба прийняти всі дані — і вік, і імʼя, 
і якщо будь-яке одне з них нас не влаштовує, 
тоді слід перепитати і вік, і імʼя.
Це погіршує user experience, ось чому: 
якщо юзер ввів вік некоректно, а імʼя - коректно, 
то програма перепитає і вік, і імʼя (хоча імʼя вже коректно введене),
але завдання саме цього і вимагає.

Також, 
натискання юзером CANCEL в завданні окремо не прописане, 
тому я на власний розсуд прирівнюю натискання CANCEL до "юзер не ввів дані"
і, відповідно, не приймаю CANCEL як валідну відповідь юзера.

Також, 
при підстановці дефолтного значення в prompt 
я перетворюю "null" на порожній рядок, адже юзер не вводив рядок "null"
і взагалі може не знати про JS та null.
*/

let userAge, userSure;
let userName = "";

// GET AGE & NAME
do {
	userAge = prompt("What is your age?", userAge == null ? "" : userAge);
	userName = prompt("What is your name?", userName == null ? "" : userName);
} while (
	(isNaN(userAge) || !userAge) || (!userName)
);

// CHECK AGE GROUP
if (userAge < 18) {
	alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
	userSure = confirm("Are you sure you want to continue?");
	if (userSure) {
		alert(`Welcome, ${userName}`);
	} else {
		alert("You are not allowed to visit this website");
	}
} else {
	alert(`Welcome, ${userName}`);
}
